'use strict';

/**
 * @ngdoc service
 * @name jwtCoursePsApp.authToken
 * @description
 * # authToken
 * Factory in the jwtCoursePsApp.
 */
angular.module('kjwt')
  .factory('authToken', ['$window', function ($window) {
    var storage = $window.localStorage;
    var cachedToken;

    return {
      setToken: function(token) {
        cachedToken = token;
        storage.setItem('userToken', token);
        this.isAuthenticated = true;
      },

      getToken: function() {
        if(!cachedToken) {
          cachedToken = storage.getItem('userToken');

          return cachedToken;
        }
      },

      isAuthenticated: function() {
        if(this.getToken()) {
          return true;
        } else {
          return false;
        }
      },

      removeToken: function() {
        cachedToken = null;
        storage.removeItem('userToken');
      }
    };
  }]);
