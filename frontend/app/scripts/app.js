'use strict';

/**
 * @ngdoc overview
 * @name jwtCoursePsApp
 * @description
 * # jwtCoursePsApp
 *
 * Main module of the application.
 */
angular
  .module('kjwt', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router'
  ])

  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('jobs', {
        url: '/',
        templateUrl: '/views/jobs.html',
        controller: 'JobsCtrl'
      })
      .state('register', {
        url: '/register',
        templateUrl: '/views/pages/auth/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'vm'
      })
      .state('logout', {
        url: '/logout',
        controller: 'LogoutCtrl'
      });

    $urlRouterProvider.otherwise('/');
  }])

  .constant('API_URL', 'http://localhost:3000/');
