'use strict';

/**
 * @ngdoc function
 * @name jwtCoursePsApp.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the jwtCoursePsApp
 */
angular.module('kjwt')
  .controller('RegisterCtrl', ['$scope', '$http', 'authToken', function ($scope, $http, authToken) {
    var vm = this;
    vm.formData = {};
    var url = 'http://localhost:3000/register';

    vm.submit = function() {
      $http.post(url, vm.formData)
        .success(function (data) {
          console.log(data);

          authToken.setToken(data.token);
        })
        .error(function(err) {

        });
    }
  }]);
