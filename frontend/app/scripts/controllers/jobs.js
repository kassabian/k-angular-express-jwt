'use strict';

/**
 * @ngdoc function
 * @name jwtCoursePsApp.controller:JobsCtrl
 * @description
 * # JobsCtrl
 * Controller of the jwtCoursePsApp
 */
angular.module('kjwt')
  .controller('JobsCtrl', ['$scope', '$http', 'API_URL', function ($scope, $http, API_URL) {
    $http.get(API_URL + 'jobs').then(function (data) {
      $scope.jobs = data;
    }, function(err) {
      console.log(err);
      alert(err.data.message);
    });
  }]);
