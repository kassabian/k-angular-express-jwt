'use strict';

/**
 * @ngdoc function
 * @name jwtCoursePsApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the jwtCoursePsApp
 */
angular.module('kjwt')
  .controller('LogoutCtrl', ['authToken', '$state', function (authToken, $state) {
    authToken.removeToken();
    $state.go('jobs');
  }]);
