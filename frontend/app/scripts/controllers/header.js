'use strict';

/**
 * @ngdoc function
 * @name jwtCoursePsApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the jwtCoursePsApp
 */
angular.module('kjwt')
  .controller('HeaderCtrl', ['$scope', 'authToken', function ($scope, authToken) {
    $scope.isAuthenticated = authToken.isAuthenticated();
  }]);
